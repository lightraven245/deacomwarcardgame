﻿Author: Max Hale
Purpose: Deacom Screening
Date of Creation: 9/18/2017

Instructions:
To play the game simply start the executable by typing ".\warCardGameConsole.exe" via Powershell, Cmd, etc

Configuration:
Adjust autoplay speed via "warCardGameConsole.exe.config"