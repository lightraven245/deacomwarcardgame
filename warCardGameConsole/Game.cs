﻿using System;
using System.Linq;
using System.Threading;
using System.Configuration;
using warCardGameConsole.Objects;

namespace warCardGameConsole
{
    class Game
    {
        // Player Decks
        private Deck playerOneDeck;
        private Deck playerTwoDeck;

        // Reserve Decks (for storing won/acquired cards)
        private Deck playerOneReserveDeck = new Deck();
        private Deck playerTwoReserveDeck = new Deck();

        // War Deck (to hold all cards placed into a war)
        private Deck warDeck = new Deck();

        private const int halfDeckSize = 26;
        private bool autoPlay = false;
        private bool isGameOver = false;

        // Config Option(s)
        private int autoPlayDelay = int.Parse(ConfigurationManager.AppSettings["autoPlayDelay"]);
        public void PlayGame()
        {
            PrintIntro();
            InitializeDecks();
            War();
            Console.WriteLine("Game is over, thanks for playing!.");
        }
        public void InitializeDecks()
        {
            // Create deck of 52 unshuffled cards before distributing across two hands
            Deck fullUnshuffledDeck = new Deck();
            foreach(Suit s in Enum.GetValues(typeof(Suit)))
            {
                foreach (CardRanking ranking in Enum.GetValues(typeof(CardRanking)))
                    fullUnshuffledDeck.AddCard(new Card(s, ranking));
            }

            // Shuffle deck and distribute to player hands
            fullUnshuffledDeck.Shuffle();
            
            playerOneDeck = new Deck(fullUnshuffledDeck.GetCards().Take(halfDeckSize).ToList());
            playerTwoDeck = new Deck(fullUnshuffledDeck.GetCards().Skip(halfDeckSize).Take(halfDeckSize).ToList());
        }

        // Main function for handling the game
        public void War()
        {
            Card playerOneCurrentCard = null;
            Card playerTwoCurrentCard = null;

            while (!isGameOver)
            {
                MergeReserveAndPrimaryDecks();
                if (!autoPlay)
                {
                    HandleUserInput();
                }
                else
                {
                    // If autoplay == true then each turn will be automatically played out at a default of .5 seconds
                    // This is configurable via the app.config
                    Thread.Sleep(autoPlayDelay);
                }

                playerOneCurrentCard = playerOneDeck.DealCard();
                playerTwoCurrentCard = playerTwoDeck.DealCard();

                PrintCurrentPlay(playerOneCurrentCard, playerTwoCurrentCard);
                EvaluateCurrentMove(playerOneCurrentCard, playerTwoCurrentCard);

                CheckVictoryConditions();
            }
        }

        public void EvaluateCurrentMove(Card playerOneCurrentCard, Card playerTwoCurrentCard, bool isWar = false)
        {
            if (playerOneCurrentCard.GetRanking() > playerTwoCurrentCard.GetRanking())
            {
                // Player One Wins
                Console.WriteLine("\nPlayer One wins this hand.");
                playerOneReserveDeck.AddCard(playerOneCurrentCard);
                playerOneReserveDeck.AddCard(playerTwoCurrentCard);

                if (isWar)
                {
                    MergeWarDeck(playerOneReserveDeck);
                }
            }
            else if (playerOneCurrentCard.GetRanking() == playerTwoCurrentCard.GetRanking())
            {
                // WAR
                Console.WriteLine("\nPlayers have tied. War is declared!!\n");
                for (int i = 0; i < 3; i++)
                {
                    MergeReserveAndPrimaryDecks();
                    CheckVictoryConditions();
                    if (isGameOver)
                        return;
                    warDeck.AddCard(playerOneDeck.DealCard());
                    warDeck.AddCard(playerTwoDeck.DealCard());
                }

                MergeReserveAndPrimaryDecks();
                CheckVictoryConditions();
                if (isGameOver)
                    return;
                playerOneCurrentCard = playerOneDeck.DealCard();
                playerTwoCurrentCard = playerTwoDeck.DealCard();

                PrintCurrentPlay(playerOneCurrentCard, playerTwoCurrentCard);
                EvaluateCurrentMove(playerOneCurrentCard, playerTwoCurrentCard, true);
            }
            else
            {
                // Player Two Wins
                Console.WriteLine("\nPlayer Two wins this hand.");
                playerTwoReserveDeck.AddCard(playerOneCurrentCard);
                playerTwoReserveDeck.AddCard(playerTwoCurrentCard);

                if (isWar)
                {
                    MergeWarDeck(playerTwoReserveDeck);
                }
            }

        }

        // Used to distribute war winnings into respective player's deck
        private void MergeWarDeck(Deck playerReserveDeck)
        {
            Console.WriteLine("Merging war deck into winners reserve.");
            foreach (Card card in warDeck.GetCards())
                playerReserveDeck.AddCard(card);
            warDeck = new Deck();
        }

        private void MergeReserveAndPrimaryDecks()
        {
            // Evaluate check on player one
            if (playerOneDeck.IsEmpty() && !playerOneReserveDeck.IsEmpty())
            {
                Console.WriteLine("Player One merging reserve deck.");
                playerOneDeck = new Deck(playerOneReserveDeck.GetCards());
                playerOneDeck.Shuffle();
                playerOneReserveDeck = new Deck();
            }

            // Evaluate check on player two
            if (playerTwoDeck.IsEmpty() && !playerTwoReserveDeck.IsEmpty())
            {
                Console.WriteLine("Player Two merging reserve deck.");
                playerTwoDeck = new Deck(playerTwoReserveDeck.GetCards());
                playerTwoDeck.Shuffle();
                playerTwoReserveDeck = new Deck();
            }
        }

        private void CheckVictoryConditions()
        {
            if (playerOneDeck.IsEmpty() && playerOneReserveDeck.IsEmpty())
            {
                Console.WriteLine("Player One has lost!!!");
                isGameOver = true;
            }
            else if (playerTwoDeck.IsEmpty() && playerTwoReserveDeck.IsEmpty())
            {
                Console.WriteLine("Player Two has lost!!!");
                isGameOver = true;
            }
        }


        private void PrintIntro()
        {
            Console.WriteLine("Welcome to War!");
            Console.WriteLine("Created by: Max Hale");
            Console.WriteLine("Purpose: Deacom Screening");
            Console.WriteLine("*****************************\n\n");
        }

        private void HandleUserInput()
        {
            string response = PromptUserAction();
            if (response.Equals("d"))
            { //no action, just an illusion of control :)
            }
            else if (response.Equals("a"))
                autoPlay = true;
            else if (response.Equals("q"))
                Environment.Exit(1);
            else
            {
                Console.WriteLine("Response unrecognized, please try again.");
                HandleUserInput();
            }
        }

        private string PromptUserAction()
        {
            Console.WriteLine("\n\n***********************************************");
            Console.WriteLine("Perform an action: (d) - Deal Card, (a) - AutoPlay {0}ms sleep between turns, (q) - Quit", autoPlayDelay);
            return Console.ReadLine().ToLower(); 
        }

        private void PrintCurrentPlay(Card playerOneCurrentCard, Card playerTwoCurrentCard)
        {
            Console.WriteLine("Player One Played:");
            playerOneCurrentCard.Display();
            Console.WriteLine("Player Two Played:");
            playerTwoCurrentCard.Display();
        }
    }
}
