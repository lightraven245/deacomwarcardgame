﻿namespace warCardGameConsole.Objects
{
    public enum CardRanking
    {
        Two=2,
        Three=3,
        Four=4,
        Five=5,
        Six=6,
        Seven=7,
        Eight=8,
        Nine=9,
        Jack=10,
        Queen=11,
        King=12,
        Ace=13
    }
}
