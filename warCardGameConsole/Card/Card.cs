﻿using System;

namespace warCardGameConsole.Objects
{
    public class Card
    {
        private Suit suit;
        private CardRanking rank;

        public Card(Suit suit, CardRanking rank)
        {
            this.suit = suit;
            this.rank = rank;
        }
        public void Display()
        {
            Console.WriteLine("{0} of {1}", rank.ToString(), suit.ToString());
        }

        public CardRanking GetRanking()
        {
            return rank;
        }
    }
}
