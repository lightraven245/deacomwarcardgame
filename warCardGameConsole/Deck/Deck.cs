﻿using System;
using System.Collections.Generic;
using System.Linq;
using warCardGameConsole.Objects;

namespace warCardGameConsole
{
    public class Deck
    {
        private List<Card> cards = new List<Card>();

        public Deck()
        {

        }
        public Deck(List<Card> cards)
        {
            this.cards = cards;
        }

        public void AddCard(Card card)
        {
            cards.Add(card);
        }

        public List<Card> GetCards()
        {
            return cards;
        }
        public void Shuffle()
        {
            var random = new Random();
            cards = cards.OrderBy(c => random.Next()).ToList();
        }

        public Card DealCard()
        {
            if (cards.Count != 0)
            {
                Card card = cards.First();
                cards.Remove(card);
                return card;
            }
            return null;
        }

        public bool IsEmpty()
        {
            return cards.Count() <= 0 ? true : false;
        }

        public void Display()
        {
            cards.ForEach(c => c.Display());
        }
    }
}
